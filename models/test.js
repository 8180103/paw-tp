const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var TestSchema = new Schema({
    date: {
        type: Date,
        required: true
    },
    client: {
        type: String,
        required: true
    },
    technician: {
        type: String,
        required: true
    },
    //0 não infetado, 1 supeito, 2 infetado 
    utilstatus: {
        type: Number,
        min: 0,
        max: 2,
        required: false
    },
    teststatus: {
        type: String,
        required: false
    },
    //0 positivo, 1 negativo
    result: {
        type: Number,
        min: 0,
        max: 1,
        required: false
    },
    userid: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: false
    },
    priority: {
        type: Number,
        required: true,
        min: 0,
        max: 5
    }
});

module.exports = mongoose.model('Test', TestSchema);