var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RequestTestSchema = new Schema({
    date: {
        type: Date,
        required: true
    },
    client: {
        type: String,
        required: false
    },
    technician: {
        type: String,
        required: false
    },
    //0 não infetado, 1 supeito/infetado 
    utilstatus: {
        type: Number,
        required: false
    },
    //0 sendo analizado, 1 analise final
    teststatus: {
        type: String,
        required: false
    },
    //0 positivo, 1 negativo
    result: {
        type: Number,
        required: false
    },
    userid: {
        type: String,
        required: false
    },
    technicianid: {
        type: String,
        required: false
    }
});


module.exports = mongoose.model('RequestTest', RequestTestSchema);