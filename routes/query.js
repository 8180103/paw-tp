const express = require('express');
const router = express.Router();
const Verification = require("../routes/verifyTokens.js");
const Request = require('../models/request');
const Test = require('../models/test');

//filtros para request
router.get('/request', (req, res, next) => { Verification(req, res, next, process.env.userrole ) }, function (req, res) {

    Request.find(req.query, function (err, requests) {
        if (err) {
            return res.status(400).json("Erro!");
        } else {
            return res.status(200).json(requests);
        }
    });
});

//filtros para teste
router.get('/test', (req, res, next) => { Verification(req, res, next, tecnicrole) }, function (req, res) {
    Test.find(req.query, function (err, tests) {
        if (err) {
            return res.status(400).json("Erro!");
        } else {
            return res.status(400).json(tests);
        }
    });
});

module.exports = router;