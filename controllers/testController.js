const mongoose = require('mongoose');
const mongodb = require('mongodb');
const Test = require("../models/test");
const UserInfo = require("../models/userInfo");

var testController = {};

/**Criar teste, sem pedido, não é recomendado*/
testController.createTest = function (req, res, next) {

    var test = new Test();
    Test.find({ userid: req.body.userid }, function (err, tests) {
        if (err) {
            next(err);
        } else {
            var date = Date.parse(req.body.date);
            var mais = addMin(2880, date);
            var menos = takeMin(2880, date);
            const verificationDate = verifyDate(tests, menos, mais);
            if (verificationDate) {
                res.json("Impossivel marcar a essa hora");
            } else {
                UserInfo.findById({ _id: req.body.userid }, function (err, user) {
                    if (err) {
                        next(err);
                    } else {
                        test.utilstatus = user.status;
                        test.code = generateCode();
                        test.save(function (err) {
                            if (err) {
                                next(err);
                            } else {
                                res.json(test);
                            }
                        });
                    }
                });
            }

        }
    });
}

testController.getFile = function (req, res, next) {
    if (req.test.code == undefined)
        return res.status(404).json({ message: "File not exist" });

    let fileName = req.test.code;
    const url = process.env.DB_CONNECTION;
    //Connect to the MongoDB client

    mongodb.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
        if (err) {
            return res.status(500).json({ message: "Error in Retriving File" });
        }

        const db = client.db("test");
        const collection = db.collection("fs.files");
        const collectionChunks = db.collection("fs.chunks");

        collection.find({ filename: fileName }).toArray(function (err, docs) {
            if (err) {
                return res.status(500).json({ message: "Error finding file" });
            }
            if (!docs || docs.length === 0)
                return res.status(500).json({ message: "Not find any file" });
            else {
                //Retrieving the chunks from the db
                collectionChunks
                    .find({ files_id: docs[0]._id })
                    .sort({ n: 1 })
                    .toArray(function (err, chunks) {
                        if (err) {
                            return res.status(500).json("index", {
                                title: "Download Error",
                                message: "Error retrieving chunks",
                                error: err.errmsg,
                            });
                        }
                        if (!chunks || chunks.length === 0) {
                            //No data found
                            return res.status(500).json("index", {
                                title: "Download Error",
                                message: "No data found",
                            });
                        }

                        res.set("Content-Type", docs[0].contentType);
                        return res.send(chunks[0].data.buffer);
                    });
            }
        });
    });
}

/**Get All Tests */
testController.getAllTests = function (req, res, next) {
    Test.find(function (err, tests) {
        if (err) {
            next(err);
        } else {
            res.json(tests);
        }
    });
};

/**Encontra o test através do id */
testController.getByIdTest = function (req, res, next, id) {
    Test.findOne({ _id: id }, function (err, test) {
        if (err) {
            next(err);
        } else {
            req.test = test;
            next();
        }
    });
};

/**Encontra o test através do codigo */
testController.getByCodeTest = function (req, res, next, code) {
    Test.findOne({ code: code }, function (err, test) {
        if (err) {
            next(err);
        } else {
            req.test = test;
            next();
        }
    });
};

/**Get One Test*/
testController.getOneTest = function (req, res, next) {
    res.json(req.test);
};

/** Updating Test Info*/
testController.updateTest = function (req, res, next) {
    if (req.body.date != null) {

        var newdate = Date.parse(req.body.date);
        var date = Date.parse(req.test.date);

        if (newdate > date) {
            if (req.body.result == null) {
                Test.findByIdAndUpdate(req.test._id, req.body, { new: true })
                    .then(res.json("Dados alterados com sucesso!"));
            } else {
                res.json("Não atualize a data e o resultado ao mesmo tempo");
            }
        } else {
            res.json("Coloque uma data mais adiantada");
        }
    } else {
        if (req.body.result) {
            Test.find({ userid: req.test.userid }, function (err, tests) {
                if (err) {
                    next(err);
                } else {
                    if (tests.length > 1 && tests[tests.length - 2].result == 1 && tests[tests.length - 1].result == 1) {
                        UserInfo.findOneAndUpdate(req.test.userid, { status: 0 }, { new: true }, function (err) {
                            if (err) {
                                next(err);
                            } else {
                                Test.findByIdAndUpdate(req.test._id, req.body, { new: true }, function (err) {
                                    if (err) {
                                        res.status(400).json(err);
                                    }
                                    else res.status(200).json("Dados alterados com sucesso");
                                });
                            }
                        })
                    } else if (tests.length == 1 && req.body.result == 1) {
                        //ATUALIZA TEST
                        Test.findByIdAndUpdate(req.test._id, req.body, { new: true },
                            function (err) {
                                if (err) {
                                    next();
                                } else {
                                    //CRIA UM NOVO TESTE
                                    var date2 = addDays(req.test.date, 2);
                                    var temp = {
                                        date: date2,
                                        userid: req.test.userid,
                                        client: req.test.client,
                                        technician: req.test.technician
                                    };

                                    UserInfo.findById({ _id: req.test.userid }, function (err, user) {
                                        if (err) {
                                            next(err);
                                        } else {
                                            var test2 = new Test(temp);
                                            test2.utilstatus = user.status;
                                            test2.code = generateCode();
                                            test2.save(function (err) {
                                                if (err) {
                                                    next(err);
                                                } else {
                                                    res.json(test2);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                    } else {
                        UserInfo.findByIdAndUpdate(req.test.userid, { status: 2 }, { new: true }, function (err) {
                            if (err) {
                                next(err);
                            } else {
                                Test.findByIdAndUpdate(req.test._id, req.body, { new: true }, function (err) {
                                    if (err) {
                                        res.status(400).json(err);
                                    }
                                    else res.status(200).json("Dados alterados com sucesso");
                                });
                            }
                        });
                    }
                }
            });
        } else {
            Test.findByIdAndUpdate(req.test._id, req.body, { new: true }, function (err) {
                if (err) {
                    res.status(400).json(err);
                }
                else res.status(200).json("Dados alterados com sucesso");
            });
        }
    }
};

/**Delete Test*/
testController.deleteTest = function (req, res, next) {
    req.test.remove(function (err) {
        if (err) {
            next(err);
        } else {
            res.json(req.test);
        }
    });
};

/**Seus próprios testes */
testController.myTests = function (req, res, next) {
    Test.find({ userid: req.userid }, function (err, tests) {
        if (err) {
            next(err);
        } else {
            res.json(tests);
        }
    });
};

/**Testes de um user dando como param o seu id */
testController.userTests = function (req, res, next) {
    Test.find({ userid: req.user._id }, function (err, tests) {
        if (err) {
            next(err);
        } else {
            res.json(tests);
        }
    });
};

/**Verifica se existe um teste proximo do dado */
function verifyDate(tests, menos, mais) {
    for (var i = 0; i < tests.length; i++) {
        if (Date.parse(tests[i].date) >= menos && Date.parse(tests[i].date) <= mais) {
            return true;
        }
    }
    return false;
};

/**Adiciona minutos a uma data*/
function addMin(min, date) {
    date = date + (min * 60 * 1000);
    return date;
};

/**Tira minutos a uma data*/
function takeMin(min, date) {
    date = date - (min * 60 * 1000);
    return date;
};

/**Adiciona dias a uma data*/
function addDays(date, days) {
    var date = new Date();
    date.setDate(date.getDate() + days);
    return date
};

/**Gera uma string aleatória*/
function generateCode() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

module.exports = testController;

